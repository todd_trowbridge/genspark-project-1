package com.toddtrowbridge;
// import Scanner for console input
import java.util.Scanner;
// import TimeUnit to make arcade style text
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
	    // use slowPrint instead of System.out.print(c) for slowly appearing text
        slowPrint("\n");
        slowPrint("Dragon Cave® - By Todd Trowbridge \n");
        slowPrint("\tYou are in a land full of dragons. \n");
        slowPrint("\tIn front of you there are two caves. \n");
        slowPrint("\tIn one cave the dragon is friendly and will share his treasure with you. \n");
        slowPrint("\tThe other dragon is greedy and will eat your face off and save your bones for toothpicks. \n");
        slowPrint("\t");
        while (true){
            slowPrint("Which cave will you enter? (1 or 2) \n \t");
            // create new scanner to get console input
            Scanner getInput = new Scanner(System.in);
            // use scanner to get input from next line
            String input = getInput.nextLine();
            if (input.equals("1")){
                // story text
                slowPrint("\tYou approach the cave... \n");
                slowPrint("\tIt is dark and spooky... \n");
                slowPrint("\tA large dragon appears in front of you... \n");
                slowPrint("\tShe opens her jaws and bites you in half. \n");
                slowPrint("\tYou utter \"T\'is but a flesh wound\" \n");
                slowPrint("\tYou died poor and alone.\n\n");
                slowPrint("\tGame over");
                // exit out of loop
                break;
            }
            if (input.equals("2")){
                // story text
                slowPrint("\tYou approach the cave... \n");
                slowPrint("\tIt is dark and spooky... \n");
                slowPrint("\tA large dragon appears in front of you... \n");
                slowPrint("\tShe opens her tiny hand and points to a chest overflowing with gold. \n");
                slowPrint("\tYou quickly gather as much as you can carry... \n");
                slowPrint("\tRunning towards the exit you scream \"thank you\" in a ridiculously high pitched voice. \n");
                slowPrint("\tYou're rich!!\n\n");
                slowPrint("\tGame Over.");
                // exit out of loop
                break;
            }
            // catch any character that's not a 1 or 2 and start loop over
            slowPrint("\tPlease enter 1 or 2, geez! \n\t");
        }
    }

    // the following method prints to the console with a delay between each character
    // shamelessly stolen from: https://replit.com/talk/learn/Slow-Print-tutorial-for-JAVA/51697
    // since I didn't write this I'm going to break down what I think is happening
    public static void slowPrint(String output) {
        // for loop that runs for each character in the method input string
        for (int i = 0; i < output.length(); i++) {
            // grab the character at position i
            char c = output.charAt(i);
            // print the single character
            System.out.print(c);
            // wrapped in a try catch statement
            try {
                // pause the for loop for X milliseconds
                TimeUnit.MILLISECONDS.sleep(25);
            }
            // catch exception
            catch (Exception e) {
                System.out.println("something broke");
            }
        }
    }
}
